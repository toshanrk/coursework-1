#include <iostream>
#include <fstream>
#include <string>
#include "Event.h"

using namespace std;

extern vector<Film> films;
extern vector<LiveMusic> livemusics;
extern vector<StandUpComedy> comedies;
extern vector<Event> events;
extern vector<Booking> bookings;
extern int bookingID;

void loadData()
{
    //load booking count id
    ifstream bookingdata("bookings.txt");
    string data;
    bookingdata >> data >> data;
    bookingID = stoi(data);
    //cout << bookingID << endl;
    int id, number;
    string name, type, seats;
    string ignore;
    while (!bookingdata.eof()) {
        bookingdata >> ignore >> id >> ignore >> name >> ignore >> type >> ignore >> seats >> ignore >> number;
        if (!bookingdata.eof()) {
            if (id != -1) {
                Booking booking;
                booking.id = id;
                booking.name = name;
                booking.type = type;
                booking.numberOfCustomers = number;
                if (seats == "-1") booking.seatNumbers = "nil";
                else booking.seatNumbers = seats;

                bookings.push_back(booking);
            }
        }
    }
    bookingdata.close();



    ifstream datainput("data.txt");
    string line;
    string quality, seatings;
    int capacity, available;

    while (!datainput.eof())
    {
        datainput >> ignore >> name >> ignore >> capacity >> ignore >> available >> ignore >> type >> ignore >> quality >> ignore >> seatings;
        if (!datainput.eof())
        {
            //cout << name << " " << capacity << " " << available << " " << type << " " << quality << " " << seatings << endl;
            Event event;
            event.name = name;
            event.type = type;
            events.push_back(event);

            if (type == "film")
            {
                Film film;
                film.name = name;
                film.capacity = capacity;
                film.available = available;
                film.quality = quality;

                films.push_back(film);
            }
            else if (type == "comedy")
            {
                StandUpComedy comedy;
                comedy.name = name;
                comedy.capacity = capacity;
                comedy.available = available;

                string strarray = seatings;

                for (int i = 0; i < strarray.length(); i++)
                {
                    if (strarray[i] == '1')
                    {
                        comedy.seatings[i] = true;
                    }
                    else
                        comedy.seatings[i] = false;
                }
                comedy.createSeatMap();
                comedies.push_back(comedy);
            }
            else if (type == "music")
            {
                LiveMusic music;
                music.name = name;
                music.capacity = capacity;
                music.available = available;

                livemusics.push_back(music);
            }
        }
    }

    for (int i = 0; i < films.size(); i++) {
        cout << films[i].name << films[i].type << endl;
    }

    datainput.close();
}

void saveData() {
    ofstream outdata("data.txt");
    string ignore;
    string line;
    string name, type, quality, seatings;
    int capacity, available;

    for (int i = 0; i < films.size(); i++) {
        name = films[i].name;
        quality = films[i].quality;
        seatings = "nil";
        capacity = 200;
        available = films[i].available;

        outdata << "name: " << name << endl;
        outdata << "capacity: " << capacity << endl;
        outdata << "available: " << available << endl;
        outdata << "type: " << "film" << endl;
        outdata << "quality: " << quality << endl;
        outdata << "seatings: " << seatings << endl << endl;;
    }

    for (int i = 0; i < livemusics.size(); i++) {
        name = livemusics[i].name;
        quality = "nil";
        seatings = "nil";
        capacity = 300;
        available = livemusics[i].available;

        outdata << "name: " << name << endl;
        outdata << "capacity: " << capacity << endl;
        outdata << "available: " << available << endl;
        outdata << "type: " << "music" << endl;
        outdata << "quality: " << "nil" << endl;
        outdata << "seatings: " << seatings << endl << endl;;
    }

    for (int i = 0; i < comedies.size(); i++) {
        name = comedies[i].name;
        quality = "nil";
        seatings = "";
        capacity = 200;
        available = comedies[i].available;

        for (int j = 0; j < sizeof(comedies[i].seatings); j++) 
            seatings += comedies[i].seatings[j] ? "1" : "0";

        outdata << "name: " << name << endl;
        outdata << "capacity: " << capacity << endl;
        outdata << "available: " << available << endl;
        outdata << "type: " << "comedy" << endl;
        outdata << "quality: " << "nil" << endl;
        outdata << "seatings: " << seatings << endl << endl;
    }  

    outdata.close();


    //save bookings
    ofstream bookingdata("bookings.txt");
    bookingdata << "bookingCount: " << bookingID << endl << endl; 
    for (int i = 0; i < bookings.size(); i++) {
        int id, number;
        string seats;
        
        id = bookings[i].id;
        name = bookings[i].name;
        type = bookings[i].type;
        seats = bookings[i].seatNumbers;
        number = bookings[i].numberOfCustomers;
        
        
        bookingdata << "id: " << id << endl;
        bookingdata << "name: " << name << endl;
        bookingdata << "type: " << type << endl;
        bookingdata << "seats: " << seats << endl;
        bookingdata << "number: " << number << endl << endl;
    }
    bookingdata.close();
}
