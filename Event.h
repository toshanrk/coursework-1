#include <iostream>
#include <vector>

using namespace std;

class Event {
    public:
        string name;
        int capacity;
        int available;
        string type;
};

class LiveMusic : public Event {
    
};

class StandUpComedy : public Event {
    public:
        bool seatings[200] = {false};
        string seatMap;

        void createSeatMap() {
            for (int j = 0; j < sizeof(seatings); j++) {
                    if (j % 10 == 0) seatMap += "\n | ";
                    seatMap += seatings[j] ? "Seat " + to_string(j + 1) + " - A | " : "Seat " + to_string(j + 1) + " - O | ";
            }
        }
};

class Film : public Event {
    public:
        string quality;
};

class Booking: public Event {
    public:
        int id;
        int numberOfCustomers;
        string seatNumbers;
};