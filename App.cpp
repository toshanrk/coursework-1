#include <iostream>
//#include "Event.h"
#include <fstream>
#include <vector>
#include "FileManager.h"
#include <limits>
#include <string>

using namespace std;

vector<Film> films{};
vector<LiveMusic> livemusics{};
vector<StandUpComedy> comedies{};
vector<Event> events{};
vector<Booking> bookings{};

int bookingID = 1;

void invalidInput() {
    if (!cin) {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
}

void listEvents()
{
    cout << "Events: \n";

    for (int i = 0; i < events.size(); i++)
    {
        cout << i + 1 << "." << events[i].name << " - " << events[i].type << endl;
    }
    cout << endl;
}

void getDetails(string name, string type)
{
    string eventName;
    int eventCapacity;
    int available;
    string seatings;
    string quality;

    if (type == "film")
    {
        for (int i = 0; i < films.size(); i++)
        {
            if (films[i].name == name)
            {
                eventName = name;
                eventCapacity = films[i].capacity;
                available = films[i].available;
                quality = films[i].quality;
            }
        }
    }
    else if (type == "comedy")
    {
        for (int i = 0; i < comedies.size(); i++)
        {
            if (comedies[i].name == name)
            {
                eventName = name;
                eventCapacity = comedies[i].capacity;
                available = comedies[i].available;
                seatings = comedies[i].seatMap;
            }
        }
    }
    else if (type == "music")
    {
        for (int i = 0; i < livemusics.size(); i++)
        {
            if (livemusics[i].name == name)
            {
                eventName = name;
                eventCapacity = livemusics[i].capacity;
                available = livemusics[i].available;
            }
        }
    }

    cout << "Event name: " << eventName << endl;
    cout << "Total Capacity: " << eventCapacity << endl;
    cout << "Available space: " << available << endl;
    if (type == "film")
        cout << "Quality of film: " << quality << endl;
    else if (type == "comedy")
    {
        cout << "Seat map:" << endl;
        cout << "O - occupied , A - available";
        cout << seatings << endl;
    }
}

void bookEvent(string name, string type) {
    //cout << name << " " << type << endl;
    if (type == "film") {
        for (int i = 0; i < films.size(); i++)
        {
            int n;
            if (films[i].name == name)
            {  
                if (films[i].available <= 0) {
                    cout << "Sorry, this event is already fully booked." << endl;
                    return;
                }
                while (true) {
                    cout << "Number of customers: ";
                    cin >> n;
                    invalidInput();

                    if (n < 1) cout << "Invalid number\n";
                    else if (n > films[i].available) cout << "There is not enough space for " << n << " customers, choose a lower number\n";
                    else break;
                }

                films[i].available-=n;
                Booking booking;
                booking.id = bookingID;
                booking.name = name;
                booking.type = type;
                booking.seatNumbers = "nil";
                booking.numberOfCustomers = n;
                bookings.push_back(booking);
                bookingID++;
                cout << endl << "Updated event details\n";
                getDetails(name, type);
                saveData();
                return;
            }
            
        }
    }
    else if (type == "music") {
        for (int i = 0; i < livemusics.size(); i++)
        {
            int n;
            if (livemusics[i].name == name)
            {
                if (livemusics[i].available <= 0) {
                    cout << "Sorry, this event is already fully booked." << endl;
                    return;
                }
                while (true) {
                    cout << "Number of customers: ";
                    cin >> n;
                    invalidInput();

                    if (n < 1) cout << "Invalid number\n";
                    else if (n > films[i].available) cout << "There is not enough space for " << n << " customers, choose a lower number\n";
                    else break;
                }

                livemusics[i].available-=n;
                Booking booking;
                booking.id = bookingID;
                booking.name = name;
                booking.type = type;
                booking.seatNumbers = "nil";
                booking.numberOfCustomers = n;
                bookings.push_back(booking);
                bookingID++;
                cout << endl << "Updated event details\n";
                getDetails(name, type);
                saveData();
                return;
            }
            
        }
    }
    else if (type == "comedy") {
        for (int i = 0; i < comedies.size(); i++)
        {
            int n;
            string seatNumbers;
            if (comedies[i].name == name)
            {
                if (comedies[i].available <= 0) {
                    cout << "Sorry, this event is already fully booked." << endl;
                    return;
                }

                while (true) {
                    cout << "Number of customers: ";
                    cin >> n;
                    invalidInput();

                    if (n < 1) cout << "Invalid number\n";
                    else if (n > comedies[i].available) cout << "There is not enough space for " << n << " customers, choose a lower number\n";
                    else break;
                }
                cout << comedies[i].seatMap << endl;
                cout << "Enter seat numbers:\n"; 
                int x;
                for (int j = 0; j < n; j++) {
                    cin >> x;
                    if (x < 1 || x > 200) {
                        cout << "Invalid seat number.\n";
                        j--;
                    }
                    else if (!comedies[i].seatings[x-1]) {
                        cout << "This seat is already occupied.\n";
                        j--;
                    }
                    comedies[i].seatings[x-1] = false;
                    seatNumbers+=to_string(x) + "-";
                }
                comedies[i].seatMap = "";
                comedies[i].createSeatMap();
                comedies[i].available -=n;
                Booking booking;
                booking.id = bookingID;
                booking.name = name;
                booking.type = type;
                booking.seatNumbers = seatNumbers;
                booking.numberOfCustomers = n;
                bookings.push_back(booking);
                bookingID++;
                cout << "Updated event details.\n";
                getDetails(name, type);
                saveData();
                return;
            }
        }
    }
}


void cancelBooking(int id) {
    string name, type, seats;
    int number;
    for (auto i = bookings.begin(); i < bookings.end(); i++) {
        if (i->id == id) {
            name = i->name;
            type = i->type;
            number = i->numberOfCustomers;
            seats = i->seatNumbers;

            bookings.erase(i);
        }
    }

    if (type == "film") {
        for (int i = 0; i < films.size(); i++) {
            if (name == films[i].name) {
                films[i].available+=number;
            }
        }
    }
    else if (type == "music") {
        for (int i = 0; i < livemusics.size(); i++) {
            if (name == livemusics[i].name) {
                livemusics[i].available+=number;
            }
        }
    } 
    else if (type == "comedy") {
        for (int i = 0; i < livemusics.size(); i++) {
            if (name == comedies[i].name) {
                comedies[i].available+=number;
                string delimiter = "-";
                string token;
                int pos = 0;
                while ((pos = seats.find(delimiter)) != string::npos) {
                    token = seats.substr(0, pos);
                    cout << token << endl;
                    seats.erase(0, pos + delimiter.length());
                    comedies[i].seatings[stoi(token)- 1] = true;
                }
                comedies[i].seatMap = "";
                comedies[i].createSeatMap();
            }
        }
    }

    saveData();
}
 
 
int main()
{
    int choice = 0;

    loadData();

    while (true)
    {
        cout << "***EVENT BOOKING***\n";
        cout << "1.Add a booking for an event.\n";
        cout << "2.Cancel and Refund a booking.\n";
        cout << "3.List events.\n";
        cout << "4.Get details of an event.\n";
        cout << "5.Exit Program.\n";
        cout << "Input your choice: \n";
        cin >> choice;

        if (choice < 1 || choice > 5)
        {
            invalidInput();
            cout << "Invalid choice.\n";
        }

        switch (choice)
        {
        case 1:
            listEvents();
            cout << "Choose event to book: " << endl;
                cin >> choice;
                invalidInput();

                if (choice < 1 || choice > events.size())
                {
                    cout << "Invalid choice.\n";
                }
                else
                    bookEvent(events[choice-1].name, events[choice-1].type);
                    break;
            break;
        case 2:
            int id;
            while (true) {
                cout << "Please enter the booking id of the event: ";
                cin >> id;
                invalidInput();

                if (id <= 0) {
                    
                    cout << "Invalid id" << endl;
                }
                else break;
            }

            cancelBooking(id);
            break;
        case 3:
            listEvents();
            break;
        case 4:
            while (true)
            {
                listEvents();
                cout << "Choice: " << endl;
                cin >> choice;
                invalidInput();

                if (choice < 1 || choice > events.size())
                {
                    invalidInput();
                    cout << "Invalid choice.\n";
                }
                else
                    getDetails(events[choice - 1].name, events[choice - 1].type);
                    break;
            }
            break;
        case 5:
            exit(1);
        default:
            break;
        }
    }

    return 0;
}